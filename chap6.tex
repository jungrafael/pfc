\chapter{Implementation}

This chapter discusses about how the planning made on \nameref{chapter5} was implemented and all the changes made during the process due to problems or simply changes in the requirements.

All the items commented in this chapter were completely done by the author, except when explicitly stated the opposite.

\section{Workstation}

To be able to start with the implementation of the project, the first necessity was obtaining and configuring the workstations.

These consisted of a clean computer, without any operating system installed. This allowed for a complete customization of the development environment tools, such as using an operational system without any restrictions and installing the best suited IDEs and other supporting software available.

There was one computer per developer available, which all the work was to be done with.

The workstations were configured with the following software:
\begin{itemize}
  \item \textbf{Ubuntu Desktop} is the GNU/Linux distribution installed. The choice of distribution was made to be generic, with an OS that any new developer would either already know or be able to get used to very quickly;
  \item \textbf{Visual Studio Code} as the main editor, since it works very good with the languages that is was going to be used with, such as Javascript and Elixir. This IDE was chosen for its ability of customization and extensions. Any developer could be able to install a new IDE and use it, if better suited;
  \item \textbf{Node + NPM}: Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine, which is required for running Javascript developed code, such as the webpack [\ref{webpack}] dev server. NPM is used for easily adding packages on the Frontend application;
  \item \textbf{PostgreSQL} is the database used in production. For replicating the production environment, the same database was installed and used in the development machines;
  \item \textbf{Erlang + kiex}: The Erlang OTP and Elixir are required for compiling and running the server in development. Kiex is an Elixir version manager, for easily changing the Elixir version on the system;
  \item \textbf{Docker} is required for testing the CI pipeline and to simplify running an Oracle database server. It would ease the development of tools to integrate with Rolls-Royce's Oracle databases.
\end{itemize}

By using similar setups in all workstations, the risk of coming to problems in the development environment is lower, since the variability is reduced. Also, setting up a new software is easier, since the process would be the same for all workstations.

\section{Repository}

  To start the well spoken application development, first the repository needs to be initialized. This consists of configuring all pieces of software to work together and setting up the automated applications deployment pipelines.

  \subsection{Base project setup}

  The base project is basically all boilerplate code necessary with setting up all the languages and build processes together. This defines the development workflow and the technology stack for the application.

  With the complete technology stack already defined in \nameref{chap5:technology_stack}, there were basically 3 repositories which needed to be setup and configured to work together:

  \begin{itemize}
    \item Phoenix Application: Backend API server and Main Application;
    \item Rails API: Connected to the main application using a GenServer;
    \item React Frontend: Connected using webpack dev server in development and independent from the main application in production, by producing the releases independently.
  \end{itemize}

    \subsubsection{Phoenix application}

    As described in \ref{phoenix_framework}, the Phoenix Framework is built using Elixir and its libraries. It is installed by running:
    \begin{minted}{bash}
$ mix archive.install \
  https://github.com/phoenixframework/archives/raw/master/phx_new.ez
    \end{minted}
    After having elixir and phoenix installed, the project can be created. Phoenix by default comes integrated with ``brunch'', which is the frontend assets manager and bundler. Since the frontend application needs to be independent from Phoenix, there is no need to include brunch in the project. This backend project is then an API-only application. To create such a project, we can use the command line helpers from Phoenix and with the following command, create the project:
    \begin{minted}{bash}
$ mix phx.new engino --no-brunch
    \end{minted}

    This creates the following folder structure:
    \begin{minted}{bash}
$ ls
README.md       config          lib             mix.lock        test
_build          deps            mix.exs         priv
    \end{minted}

    After the creating the initial repository, the GraphQL server library needs to be installed. To install the Absinthe library (GraphQL server) from the elixir package manager ``mix'', we need to add it to the package managements file, specifying the library and the version:
\begin{minted}{elixir}
{:absinthe_plug, "~> 1.4.4"},
{:absinthe_ecto, "~> 0.1.3"},
\end{minted}
    Then with the following command, the packages are obtained:
\begin{minted}{bash}
$ mix deps.get
\end{minted}

    To finish installation, the Absinthe needs to be included in the Phoenix routing file, by adding this to the ``router.exs'':
\begin{minted}{elixir}
forward "/graphql", Absinthe.Plug, schema: Engino.Schema
forward "/graphiql", Absinthe.Plug.GraphiQL, schema: Engino.Schema
\end{minted}

    To finish the configuration, the database connections were set up. This is done by simply setting the variables in the config files in Phoenix. Due to its irrelevance, this configuration will not be shown here.

    \subsubsection{React Frontend (Webpack)}

    A big requirement is that the React applications would be completely independent on the Backend application. That being said, we should not rely on any backend tools to create the initial files. The handy \inlineslackstyle{create-react-app} command line helper was used instead.

    For frontend and backend separation, all frontend source files were put inside a folder called \inlineslackstyle{frontend}. Other relevant files are packages list \inlineslackstyle{package.json}, the webpack configuration file \inlineslackstyle{webpack.config.js} and the folder node modules, where the installed frontend libraries reside.

    To simplify the configuration, these are going to be moved to the root of the application.

    With \inlineslackstyle{react-create-app}, the main frontend application was generated, with some files moved around to integrate better with Phoenix and make development easier:
\begin{minted}{bash}
$ npx create-react-app engino
$ mv engino/ frontend/
$ mv frontend/src frontend/app
$ mv frontend/node_modules .
$ mv frontend/package.json .
$ mv frontend/yarn.lock .
$ cat frontend/.gitignore >> .gitignore
$ rm frontend/.gitignore
$ rm frontend/README.md
\end{minted}

  After configuring the initial application, \inlineslackstyle{webpack} needs to be configured to compile the application both in production a single time and in development constantly after every change.

  Webpack can be started as a development server, which supplies all the files to the frontend on-the-fly as they are available. It was also configured to enable React's Hot Module Reloading, which enables changes to be applied in the frontend without needing to reload the application, thus making development more productive.

  The final \inlineslackstyle{webpack} configuration file is available in the attachments \nameref{appendix:webpack_configuration_file}.

  For the frontend to be included in the web server, a simple javascript file is generated and the output is written to the phoenix's assets folder \inlineslackstyle{priv/static}. Phoenix then serves this files normally as a web request.

  To simplify the development, the phoenix application can be configured to automatically start the webpack development server as soon as the phoenix server is startet, by the command \inlineslackstyle{mix phx.server}. For this, the phoenix endpoint needs to be configured as follows:

\begin{minted}{elixir}
config :engino, EnginoWeb.Endpoint,
  url: [host: System.get_env("HOST") || "localhost"],
  http: [port: 4000],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [
    node: [
      Path.expand("node_modules/webpack-dev-server/bin/
        webpack-dev-server.js"), "--no-inline", "--stdin", "--config",
        Path.expand("webpack.config.js"), "--host", "0.0.0.0"]
  ]
\end{minted}

  In the phoenix web server, the javascript is then read directly from the production generated file, if in production or from the webpack development server, if in development. This configuration is available in the frontend application's main HTML file (non relevant parts of the HTML were removed and others simplified):

\begin{minted}{erb}
<!DOCTYPE html>
<html lang="en">
  <body id="body">
    <div id="app-container">
    </div> <!-- /container -->

    <%= if Application.get_env(:engino, :environment) == :dev do %>
      <script src="http://localhost:4001/js/app.js"></script>
    <% else %>
      <script src="<%= static_path(@conn, "/js/app.js")%>"></script>
    <% end %>
  </body>
</html>
\end{minted}

  In the code, we can see the differentiation between the production and development environments. In development, the file is served through another server, which is constantly being recompiled and updated. In production, the file is completely static.

  \subsection{CI/CD}

  Having the complete application setup, the next step is to create the Continuous Integration and Continuous Delivery pipelines.

  This was developed using GitLab's integrated CI tool. It works by creating a file called \inlineslackstyle{.gitlab-ci.yml} in the root of the application, declaring all the steps of the CI script.

  The complete file is available in \nameref{appendix:gitlab_ci}.

  Basically, the CI process is divided into multiple jobs and multiple pipelines. Pipelines run sequentially and can pass files between each other and each pipeline can run multiple jobs simultaneously. For simplicity, there were created two pipelines: one for testing purposes and the other for building the release, called respectively \inlineslackstyle{test} and \inlineslackstyle{build}.

  \subsubsection{Testing pipeline}

  The purpose for the Testing pipeline, as the name says, is to really test the application, by using automated testing. Each repository developed has its own testing framework. For this reason, it makes sense to divide the testing pipeline into three different jobs:

  \begin{enumerate}
    \item Elixir backend testing: Runs all the tests created in the Elixir backend application. For this testing, Elixir's own testing framework was used (ExUnit);
    \item Rails API application testing: Runs tests on the rails application. For this, the library \inlineslackstyle{RSpec} was installed, to be used to facilitate developing the automated tests;
    \item Frontend applications testing: Run all the tests in all frontend applications. The library \inlineslackstyle{jest} was installed and configured to be used on the frontend testing. This library was chosen because of its very good integration with React and its ecosystem.
  \end{enumerate}

  By checking the configuration in \nameref{appendix:gitlab_ci}, we can see every job defined in the root of the file, where the commands each job runs inside a docker container based on the language of the test itself. For example, in Rails testing uses the Docker image \inlineslackstyle{ruby:2.4}\footnote{This Docker image is available publicly on Docker Hub - \url{https://hub.docker.com/r/_/ruby/}}, which already has everything installed and ready for testing the application. This simplifies maintenance and updating the base images.

  As the desired for integration testing it to have it testes after each code commit to the repository, this pipeline is set to run every time there is a new commit. This makes sure that problems are discovered during the development of features instead of only in the end. It also allows for knowing in which commit a specific test stopped to work, thus helping with finding the problem.

  \subsubsection{Build pipeline} \label{sssec:build_pipeline}

  In the opposite of the test pipeline, this should be run only when the code developed for the issue is done. That means, when it is merged into the master branch. Also, it should only be triggered if the tests pass. This makes sure there won't ever be any release available for deployment to the server which breaks some feature, detected by the automated testing system.

  The packing of the release is done by the help of the Elixir library Distillery. It basically compiles the whole Elixir code and packs it into a single file \inlineslackstyle{engino.tar.gz}. To be able to have the React application and the Rails application bundled together into this file, we need to make sure they are in the correct folders and ready for packaging. For the rails application, this means installing all libraries (\inlineslackstyle{gems}) into a specific folder and for the frontend, installing all packages and generating the output javascripts into the phoenix application.

  Therefore, the build process follows the following order:

  \begin{enumerate}
    \item Install Elixir libraries, by using \inlineslackstyle{mix deps.get};
    \item Install Frontend packages, by using \inlineslackstyle{yarn install --prod};
    \item Install Ruby gems (libraries), by using \inlineslackstyle{bundle install --gemfile=engino-rails-api/Gemfile --path=gems};
    \item Build Frontend applications into \inlineslackstyle{priv/static} and process them on phoenix with \inlineslackstyle{mix phx.digest};
    \item Create release file, with \inlineslackstyle{mix release};
    \item Copy generated release to a path accessible by the deployment script. In this case, Amazon S3.
  \end{enumerate}

  Since the Erlang release binaries needs to run under the same environment and it was generated, the environment where the package is generated though the scripts above needs to be the same as where the application will later run. This means, the release needs to be generated under the same environment as the production server, in this case a Red Hat Enterprise Linux 7 (RHEL7).

  Since both RHEL7 and CentOS 7 share the same architecture, and since Cent OS 7 is more publicly available, the application release can be generated under this system.

  This is very simple to achieve by using a public Docker image, already with CentOS 7 installed. Though for the generation of the release, there are many requirements, such as having Elixir, Node, Python, Ruby, Oracle Client, etc installed. This makes finding a base Docker image very hard and thus the best option is to create our own image.

  For the generation of this image, basically all the required applications needed to be installed, on top of a Cent OS 7 machine. The requirements list follows:

  \begin{itemize}
    \item Oracle Client;
    \item Python + PIP;
    \item Node.js;
    \item Yarn;
    \item Erlang/OTP;
    \item Elixir;
    \item Ruby on Rails;
    \item Extra dependencies of the above applications, such as curl, gcc, openssl, zip, git, postgresql-devel, wget, etc.
  \end{itemize}

  Taking all these requirements into consideration, the Docker image was create on top of the official CentOS 7 image, the \inlineslackstyle{centos:7}. The steps to create the image are inside the Dockerfile, which in this case basically consisted of installing all these requirements above. The complete file is in the appendix \nameref{appendix:dockerfile}

  After building this Dockerfile, the image can be pushed to GitLab's Registry \footnote{A Registry is where Docker images can be stored} and used in the CI, by declaring the job's image with

  \inlineslackstyle{image: registry.gitlab.com/jungsoft/engino/build/production:centos7}

  With this, all the commands declared in the job will be run inside the environment prepared in the Docker container.

\section{Application development}

  Only at this point, the application could in fact start to be developed. Changes in the initial repository setup were foreseen, but should only be incremental in making the interfaces better, instead of completely changes how everything was set up.

  Having the initial repository setup, the development of the application could be split into many different work fronts, which could be done simultaneously and interchangeably. The two main work fronts were the Backend and the App Frontend, which could be done completely simultaneously, due to the complete separation between them both.

  The work fronts were defined as in the Milestones planning in \nameref{chap5:issues_and_milestones_planning}, which are:
  \begin{itemize}
    \item Frontend App;
    \item Frontend Admin;
    \item Backend Elixir API;
    \item Rails Oracle connector.
  \end{itemize}

  The work fronts were started as soon as the repository was initialized and done simultaneously, by different team members. Since they were all very independent, there were no issues synchronizing the work done.

  All the work fronts cited above where developed by the author, except the Frontend App.

  The workflow done was basically to synchronize the frontend development with its backend. So as soon as a new feature in the frontend was created, a query/mutation in the GraphQL server (\inlineslackstyle{Absinthe}) was also created and added to the frontend.

  The Admin area was created as an additional app afterwards. Other applications were also added as additional frontend apps afterwards with very little effort, proving the flexibility of the platform to add new projects. There were no changes needed in the server or in the deployment process. An example of app was a standalone gallery, built reutilizing the same components from the Frontend App, developed by the same team member.

  An image of the with React developed admin area can be seen in the figure \ref{fig:engino_admin} or in the figure \ref{fig:engino_admin_black}, in respectively white and dark themes.

  \begin{figure}[ht]
    \centering
      \includegraphics[scale=0.29]{images/Engino Admin.png}
    \caption{Engino Admin \label{fig:engino_admin}}
    \legend{Admin area showing the engine view upload drag-and-drop. The new views in the software are added via this uploader.}
  \end{figure}

  \begin{figure}[ht]
    \centering
      \includegraphics[scale=0.29]{images/Engino Admin Black.png}
    \caption{Engino Admin \label{fig:engino_admin_black}}
    \legend{Admin Area in dark colors.}
  \end{figure}

  On the Backend Elixir a tool called GraphiQL was installed, together with the Absinthe library. This tool allows for executing queries in the backend, for testing purposes and is a great tool for debugging. The Figure \ref{fig:graphiql} is a screenshot from its interface.

  \begin{figure}[ht]
    \centering
      \includegraphics[scale=0.3]{images/GraphiQL.png}
    \caption{GraphiQL \label{fig:graphiql}}
    \legend{GraphiQL Tool. On the left side, is where the GraphQL query is written. On the middle, is the results and floating on the right side is the schema definition, where the API can quickly be checked by developers.}
  \end{figure}

\section{Production server} \label{sect:production_server}

  As already mentioned before, the production server is a Red Hat Enterprise Linux 7 (RHEL7). Also, there is no root access to the server available and no internet. This makes it much harder to install the application's requirements.

  \subsection{Server setup} \label{ssec:server_setup}

  Installing the application's dependencies into the server should follow the same principles as installing the application itself: we install it inside a similar environment, in an unrestricted Docker container and then copy the generated binary files to the server. This is simplified by the fact that these requirements will change very little with time, meaning a manual copy once should suffice.

  Also, the list of requirements to run the application is different as the requirements to generate it. For example, \inlineslackstyle{ImageMagick} is only required in runtime, whereas \inlineslackstyle{Node} only during the build.

  As listed in \nameref{ssec:server_software}, these requirements need to be available in the server. The only software now already installed in the server is GhostScript, all the others need to be built inside a similar environment and copied to the server. Luckily, the exact same Docker image used in the build pipeline [\ref{sssec:build_pipeline}] can be used here as well.

  The steps to generate the application binaries were taken from their respective websites and then simply run inside the Docker container.

  \subsubsection{PostgreSQL}

  For building the PostgreSQL binaries, all the requirements were already met. Then simply by running the following script the binaries were generated:

\begin{minted}{dockerfile}
# Generate PostgreSQL binaries -> Only for Binaries Generation
RUN curl \
  https://ftp.postgresql.org/pub/source/v10.1/postgresql-10.1.tar.gz \
  | tar xz && cd postgresql-10.1 \
  && ./configure --with-openssl --prefix=/opt/postgresql \
  && make -j $NUM_CPU && make install
\end{minted}

  Then, the only needed step is to package these files into a tar, send it somehow inside the Rolls-Royce network (for example using Amazon S3) and unpack in the server.

  \subsubsection{RSVG and ImageMagick}

  Very similarly to PostgreSQL, the RSVG and ImageMagick also need to be compiled and built. But in this case, some dependencies were missing, but could easily be installed in this unrestricted Docker container. The following script was able to generate the binaries:

\begin{minted}{dockerfile}
# Generate binaries for librsvg2: dependency of ImageMagick
RUN yum-builddep -y librsvg2 \
    && wget https://download.gnome.org/sources/
                    librsvg/2.40/librsvg-2.40.16.tar.xz \
    && tar xvf librsvg-2.40.16.tar.xz && cd librsvg-* \
    && ./configure --prefix=/opt/librsvg \
    && make -j $NUM_CPU && make install

# Generate ImageMagick binaries
RUN yum install -y ImageMagick-devel libpng-devel librsvg2-devel \
    ghostscript \
  && wget https://www.imagemagick.org/download/ImageMagick.tar.gz \
  && tar xvzf ImageMagick.tar.gz && cd ImageMagick-7* \
  && ./configure --prefix=/opt/imagemagick --with-png=yes \
     --with-rsvg=yes --with-gslib=yes --disable-installed \
     --disable-shared --enable-delegate-build \
  && make -j $NUM_CPU && make install
\end{minted}

  Exactly the same steps were necessary to copy the built binaries into the Rolls-Royce server.

  \subsubsection{Oracle Instant Client}

  As seen in the \nameref{appendix:dockerfile}, the Oracle Client binaries are already included. Then, we only need to package them and copy to the server, just as already done before.

  \subsection{Deployment}

  With the server completely setup, the deployment process can just assume that all requirements are already there.

  To simplify it even further, a file called \inlineslackstyle{.env} was created on the root of the application, with all the path to the software installed on \nameref{ssec:server_setup}. This file is read during application boot and all its environment variables, such as server keys and paths, are automatically set. This way, the deployment can be done without the need to worry about the server configuration.

  At this point, the deployment consists of performing the following steps:

  \begin{enumerate}
    \item Download release;
    \item Copy release to server;
    \item Stop running application;
    \item Unpack release files into the corresponding folder;
    \item Run database migrations\footnote{A change in the database structure (change tables, columns, etc) is called a migration};
    \item Start new application.
  \end{enumerate}

  Since this application needs to run inside the internal Windows Active Directory network, the easiest way is to create a simple Console Application developed in \inlineslackstyle{C\#} that will perform exactly the steps above.

  Since \inlineslackstyle{Amazon S3®} is available inside the network, this seems like a very good location to store the releases. Using C\# libraries for the connection to Amazon S3, the app is able to authenticate (via a AWS S3 C\# library), find the latest release automatically and download it.

  The connection to the server can be done via \inlineslackstyle{SSH}. There is also another \inlineslackstyle{C\#} library called \inlineslackstyle{Tamir.SharpSSH} that facilitates connecting and running commands via SSH on the server. The path inside the server, where the application needs to be deployed to is also predefined inside the application.

  With a simple \mintinline{bash}{ $ tar -xvzf file.tar.gz -C production}, where \inlineslackstyle{file.tar.gz} is the release package and \inlineslackstyle{production} is where the application will be stored, the application is updated.

  The BEAM binary release has the capability of running elixir commands via the command line and executing specific functions. We can then create a module \footnote{A module in elixir is similar to a class in object oriented languages} responsible for Deployment tasks. In this example, this module was called ReleaseTasks. This module should include all release related tasks, for example, running migrations, or any other scripts that needs to run on every deployment. To use this, one simple command is needed:

\begin{minted}{bash}
$ ./bin/engino command Elixir.Engino.ReleaseTasks migrate
\end{minted}

  By running this command, we ensure the application has its database tables up-to-date with the code.

  The BEAM binary also has a simple command to start the application as a daemon \footnote{Daemon is running the application in the background}, restart and stop. Then, simply by wrapping both commands above with a stop and then a start, we ensure the application is deployed without problems. We could also use the restart command, to have a 100\% uptime deployment. But considering the 4 commands together take only about a second to be run, this one second downtime is not a problem. There should also be noted, that when starting the application, the PATH should be set

\begin{minted}{bash}
$ ./bin/engino stop
$ PATH=$ENV_PATH ./bin/engino start
\end{minted}

\begin{figure}[ht]
  \centering
    \includegraphics[scale=0.85]{images/6.4.2 - Engino Deployer.png}
  \caption{Engino Deployer \label{fig:engino_deployer}}
\end{figure}
