\chapter{Project planning} \label{chapter5}

Project planning defined here is related to how the ``statement of work''\footnote{statement of work in this case refers basically to the list of tasks necessary to be executed, to have the problems described solved.} was created.

The planning was made mainly by the author, in conjunction with the project manager, to define the priorities and check the list of requirements.

This planning was done in three main steps:

\begin{enumerate}
  \item First all information about the requirements and constraints were collected, as listed in section \ref{chapter4};
  \item Then a deep throughout analysis was done, about the technology stack to be used and how the parts were to be connected to each other;
  \item Finally, the implementation methodology was planned: Issues and Milestones were defined and the priorities and deadlines were set.
\end{enumerate}

\section{Requirements and information compilation}

  \subsection{Engino - soft requirements} \label{ssec:engino_requirements}

  As seen in the last chapter \ref{ssec:engino}, the current application is very difficult to maintain and was created with a very inflexible codebase. The main problems with the application are inherent to the current development environment and tools. Having a solid base to develop the new application is fundamental to overcome these problems. These problems \ref{items:current_development_environment} could solved by:

  \begin{itemize}
    \item Using a Version Control System;
    \item Have a well defined and automated deployment system (use CD - Continuous Deployment - principles), with a staging (testing) server;
    \item Store all secrets in environment variables, in a unique file in the server, with very restricted permissions;
    \item Develop an automated testing system (CI - Continuous Integration), for checking all the code for new errors, upon every new block of code developed (git commit);
    \item Development inside a sandbox, with no access restrictions. This allows for much bigger productivity and code testing;
    \item Complete control on the permissions for allowing access to the code;
    \item Develop an authentication service;
    \item Modularize application, where everything should be a reusable component, both in Backend as in Frontend. This allows for much better code reuse.
  \end{itemize}

  Apart from these main problems in the application architecture and development environment, all current features and data source should be also available in the next version. This basically creates the requirements into which data sources should be imported and which/how much data should be displayed to the users.

  While taking advantage of the flexibility on the new platform, some features are also wished to be available in the next version:

  \begin{itemize}
    \item Better layout and usability;
    \item A dedicated admin page, with full control to all data in the database;
    \item New data sources, or with different formats;
    \item Better and faster searching;
    \item Marker verification, allowing only predefined authenticated people to realize the verification;
    \item Direct connection to the main Oracle database.
  \end{itemize}

  \subsection{Server - hard requirements}

  The server to which the whole application will be deployed is predefined from the IT department, with a bare metal Red Hat Enterprise Linux 7 (RHEL7) with only standard packages. Also, there will be no root access available and no internet access. All applications related to Engino should be in a single folder (sub-folders allowed) and be easily movable.

  This restriction makes the deployment process much harder to control and automate. For this reason, some scripts are necessary to overcome this limitations:

  \begin{itemize}
    \item Pre-compilation of all libraries \ref{ssec:server_software} in a container similar system (CentOS7);
    \item Package the compiled application into a single file;
    \item Deploy the single packaged file to an accessible location (not blocked by the Rolls-Royce firewall);
    \item The only machines in the network that have both access to the server and to the internet, are the common Windows® workstations. So a windows application is needed, to download the packaged application, copy to the server and run the necessary deployment scripts.
  \end{itemize}

  Details about the implementation of this deployment system will be given in the section \ref{sect:production_server}

\section{Technology stack} \label{chap5:technology_stack}

This section explains which technologies are needed and on which criteria they were selected.

\subsection{Frontend}

As seen above \ref{ssec:engino_requirements}, the platform should be set up with a flexible system, that allows the use of the newest technologies for Frontend development, but not require any specific. With this in mind, we are free to chose any library and/or framework for the Frontend (client) application, which could even be composed of multiple.

The analysis has taken into consideration the biggest four Frontend technologies to date:
\begin{itemize}
  \item AngularJS;
  \item Angular 4;
  \item React;
  \item Vue.js.
\end{itemize}

Each of these four libraries are listed below, with the factors that were considered when connecting its features with the requirements from Rolls-Royce:

\subsubsection{AngularJS}
AngularJS, or Angular 1 is the first version of the framework released. This framework has a very different approach than to the other technologies listed here, by supplying with much more features out of the box. Whereas this could be a good feature in some cases, it really limits flexibility, by being opinionated. Also, this makes it much more difficult to learn and to start with.

Another undesirable features the framework are for example:
\begin{itemize}
  \item Two-way data-binding: makes it really hard for debugging and managing state in bigger applications
  \item No virtual DOM: all changes are made directly into the HTML, making it slow when too many changes are happening
\end{itemize}

While this was the first option considered, since it is present in many other systems at Rolls-Royce, due to its lack of flexibility and problems above, it was also the first one to be discarded.

\subsubsection{Vue.js}
Vue is also a newer view library, with principles very similar to React, but a little more opinionated, meaning it also packages some other libraries for state management, routing and others.

But since the library is not backed by any big company and has seen less activity as the other libraries, its use for such a project was considered too risky. For this reason, it was the second to have the use discarded.

\subsubsection{Angular 4}

Angular 4 is actually the next version of Angular 2, which is a complete rewrite of AngularJS (Angular 1). For this reason, the analysis between Angular 1 and 4 is made here completely independent. Many of the problems existent in the first version of the library were solved, many others are still there:
\begin{itemize}
  \item Compatibility with TypeScript 2.1 and 2.2, increased security in type casting, as well as increased speed of the ngc-Compiler. Strict typing, requires less attention on arguments and variables, leading to a decreased chance for errors caused by lack of attention;
  \item MVVM architecture. This template for building applications allows to associate elements of the View with the properties and events of the Model;
  \item Two-way data binding. This is rather controversial, since it speeds up development but at the same time, makes the debugging and production tests harder;
  \item Problematic to use with any languages other than TypeScript. Angular 4 coders can only use the tools inherent to the ecosystem of this framework;
  \item Difficult to master. As already noted above, regardless of the version, Angular is quite difficult. This is due to the need to use the relatively low-spread TypeScript language, as well as the purely theoretical knowledge of many beginning developers in the field of OOP practices. Because of this, mastering the advanced practices can be hard.
\end{itemize}

Even tough this version has a much better approach than the Angular 1, it it a complete rewrite, so its completely not backwards compatible. This means that its use will not have any advantage with the many projects written with Angular 1 already in development at Rolls-Royce.

\subsubsection{React}

\begin{itemize}
  \item Virtual DOM. The main advantage of React, in comparison with other similar tools, is virtual DOM - a lightweight copy of a complete DOM tree. This works really well when the application does not have too many states changing but can be slow in the contrary case\footnote{In AngularJS, every change is modified in the DOM, in React only the layout changes, not the whole state};
  \item Orientation to the creation of custom user interfaces. This library is claimed by its creators as being one of the most versatile from all. It also offers developers a lot of tools for UI element creation, such as Material-UI, React-Bootstrap, React Toolbox, React Native, etc;
  \item Lack of a unified approach. The big flexibility this library provides, comes at the cost of every project having its own stack of dependencies. This factor makes it harder to follow the best practices and may not play in favor of React developers in situations where the deadline is close, or where it is necessary to find the best ways to solve current tasks in the shortest possible time.
\end{itemize}

React was considered the most flexible and convenient from all of the libraries taken into consideration. Because of the need of customization required by the project and the flexibility provided by React, this was considered the best option for this project.

\subsection{Backend}

Since the client application (Frontend) was completely decoupled from the backend technologies (server), both sides of technologies were of independent choice. The backend technologies could even be heterogeneous, using a microservices approach, where each specific part of the application (context) would be developed using a specific language and framework/technologies.

Taking into consideration the nature of the application, with requirement of good modern web frameworks, for faster development speeds and greater capabilities, the following web languages and frameworks were considered:
\begin{itemize}
  \item Python/Django;
  \item PHP with Laravel;
  \item Ruby on Rails;
  \item Elixir/Phoenix;
  \item Node.js with Express.
\end{itemize}

All of these frameworks are based on very similar paradigms and therefore present a similar programming interface. They are all based on MVC patterns, offer great community support (for libraries and problem solving) and would have all the necessary features.

The most crucial aspects were based on these key facts:
\begin{itemize}
  \item Django is already in use in other projects inside Rolls-Royce. This could make setup easier and issues in production were already known (or should have been);
  \item PHP is already installed in many of the servers on the company and the language is being also used in many projects;
  \item Ruby on Rails was the most mature framework from all in the list, with best community support, best development patterns;
  \item Elixir projects can be compiled to a dependency-free in BEAM (Erlang Virtual Machine) executable. This leads to a very simple deployment procedure. Elixir, since it runs on the Erlang VM, also has its capabilities, like built-in support building distributed, fault-tolerant applications;
  \item Node.js projects work really well with Single Page Applications, like with React.
\end{itemize}

Elixir was considered the best choice for this project because of all the new concepts it has brought, which work really well with API only Backends and for flexible application development. The language's ecosystem allows for easy development of very flexible modules to be used in a micro-services based architecture, which was very desireable. Another great feature was the ability to easily compile the whole project into a single package that is able to run dependency-free in the server. This was a very desireable feature to have in the rather strict IT environment.

The other frameworks listed above were tough not discarded to be used alongside the main Elixir application for more specific parts of the project. The project was made flexible to allow that. Along with Elixir, Ruby on Rails was planned to be used to make the connection to the Oracle database, since it had a very good library for it, which would make this much easier.

\section{Methodology}

  Given that the project's scope was not well defined and was continuously changing, an Agile Methodology, such as Scrum, was very suitable.

  In the Scrum Methodology, the project is cut in multiple pieces, which are developed together in a period of 2-4 weeks and is called a Sprint.

  The image below illustrates this process.

  \begin{figure}[ht]
    \centering
      \includegraphics[scale=0.35]{images/5 - Scrum.png}
    \caption{Scrum Methodology \label{fig:scrum}}
  \end{figure}

  \subsection{GitLab development workflow}

  GitLab is a Git-based repository manager and a powerful complete application for software development.

  With an "user-and-newbie-friendly" interface, GitLab enables to bring all parts responsible for the project into the same platform. For example, project managers can monitor the development of the software though GitLab's interface in real time, testers can report bugs by creating an ``Issue'', maintainers can give code reviews and developers can easily see their tasks and submit the code.

  The natural course of the software development process passes through 10 major steps:

  \begin{figure}[ht]
    \centering
      \includegraphics[scale=0.27]{images/5.3.1 - GitLab Workflow.png}
    \caption{GitLab Workflow \label{fig:gitlab_workflow}}
  \end{figure}

  \begin{enumerate}
    \item \textbf{IDEA}: Every new proposal starts with an idea. This can be a completely new feature, a change in an existing feature or a bug fix;
    \item \textbf{ISSUE}: The most effective way to discuss an idea is creating an issue for it. This issue is created inside the GitLab Issue Tracker;
    \item \textbf{PLAN}: Once the discussion in the issue comes to an agreement, on how it should be done, the coding starts. For prioritizing issues, there is the Issue Board;
    \item \textbf{CODE}: The actual issue is fixed, by writing code;
    \item \textbf{COMMIT}: Once we're happy with our draft, we can commit our code to a feature-branch with version control;
    \item \textbf{TEST}: With automated testing, the GitLab CI, run the testing scripts to check for errors introduced by the new code;
    \item \textbf{REVIEW}: Once our script works and our tests and builds succeeds, we are ready to get our code reviewed and approved;
    \item \textbf{STAGING}: The new feature is deployed to a testing server, with exactly the same features as the production server. There the software is checked if everything works as expected or if it still need adjustments. This is also a place to test for usability problems and issues that only happen in the production environment (because of type of real data, browser versions, etc);
    \item \textbf{PRODUCTION}: When we have everything working as it should, the code is deployed to the live production environment;
    \item \textbf{FEEDBACK}: Collect feedback about the deployed feature from real clients and see if it needs further improvement.
  \end{enumerate}

  \subsubsection{GitLab Issue Tracker}

  The Issue Tracker enables creation and management of all issues in the project. One issue can be a bug-fix, a new feature or a change in an existing feature. The issue tracker enables creation, management and reports about the project's development.

  \begin{figure}[ht]
    \centering
      \includegraphics[scale=0.35]{images/5.3.1 - GitLab Issue Tracker.png}
    \caption{GitLab Issue Tracker \label{fig:gitlab_issue_tracker}}
  \end{figure}

  \begin{figure}[ht]
    \centering
      \includegraphics[scale=0.3]{images/5.3.1 - GitLab New Issue.png}
    \caption{New Issue screen \label{fig:gitlab_issue_screen}}
  \end{figure}

  The Issue Board is a tool for planning and organizing the issues according to the project's workflow, setting priorities according to its context.

  \begin{figure}[ht]
    \centering
      \includegraphics[scale=0.45]{images/5.3.1 - GitLab Issue Board.png}
    \caption{GitLab Issue Board \label{fig:gitlab_issue_board}}
  \end{figure}

  Issues will be used for planning each work package to be developed for the applications. For example, each importer of a new source of data is a different issue as well as each part of the frontend development.

  To follow the best Agile [\ref{agile_development}] patterns, the issue creation process should also be dynamic. As soon as changes are seen to be necessary, an issue for this change should be created.

  For the bigger scope, Milestones [\ref{milestones}] should be used.

  \subsubsection{Milestones} \label{milestones}

  Milestones are are basically a tool to track the work based on a common target, in a specific date. An issue is normally part of a Milestone.

  The goal can be different for each situation, but the panorama is the same: you have a collection of issues and merge requests being worked on to achieve that particular objective. This goal can be basically anything that groups the team work and effort to do something by a deadline.

  \begin{figure}[ht]
    \centering
      \includegraphics[scale=0.35]{images/5.3.1 - GitLab Milestone.png}
    \caption{Milestones \label{fig:gitlab_milestone}}
  \end{figure}

  Milestones should be used for each bigger feature to be produced, which is comprised of multiple issues. They can be dependent of each other or not. An example of a milestone would be for example a ``First version of the mapping frontend''

  \subsubsection{Sprint Planning}

  Using GitLab, each piece of software to be developed is called an Issue. To plan a Sprint, all related Issues were grouped together and developed in the same period. As illustrated in the image \ref{fig:sprint_planning} GitLab provides a tool to make this process easier.

  \begin{figure}[ht]
    \centering
      \includegraphics[scale=0.5]{images/5 - Sprint Planning.png}
    \caption{Sprint Planning \label{fig:sprint_planning}}
  \end{figure}

  \subsubsection{CI/CD}

  Throughout this project, when we talk about CD, we are actually talking about Continuous Delivery.

  Because of the the production servers do not have no access to the internet, the repository server (GitLab.com) does not have any interaction to the production server (internal at Rolls-Royce).

  The approach used to deploy the application will be: TEST - BUILD - \faHandPointerO - DEPLOY, with the HAND icon symbolizing the manual deployment. This will be done through the help of a simple windows application, which has access to the compiled release packages and also access to the production server. Its job will be to basically copy this file to the server and run the necessary commands (move files, delete tmp directories, unpack the release, run migrations, etc), thus enabling a semi-automatic deployment.

  The images \ref{fig:CI} and \ref{fig:CI_error} are examples of a CI/CD pipeline in GitLab, showing respectively a successful test and a commit with an error, detected by the automated testing. Branches with errors are rejected and are not able to be merged into the master branch, thus avoiding future problems in the code.

  \begin{figure}[ht]
    \centering
      \includegraphics[scale=0.6]{images/5 - CI.png}
    \caption{GitLab CI Pipeline \label{fig:CI}}
  \end{figure}

  \begin{figure}[ht]
    \centering
      \includegraphics[scale=0.7]{images/5 - CI Error.png}
    \caption{Pipelines with error are rejected \label{fig:CI_error}}
  \end{figure}

  In image \ref{fig:CI_complete} the complete build process is shown, from testing to package building.

  \begin{figure}[ht]
    \centering
      \includegraphics[scale=0.7]{images/5 - CI-CD Complete Process.png}
    \caption{Complete CI/CD Pipeline \label{fig:CI_complete}}
  \end{figure}


  \subsection{Development environment}

  To avoid all the restrictions imposed by the IT, because of security and data protection, the whole project structure is planned to run outside of the main network infrastructure.

  The project repository will be hosted on the cloud, for ease of management and even allow remote teams and external companies to give consultancy for future further development. This gives a lot of flexibility, but also requires great care with the handling of data, meaning that no controlled data should ever be put into the repository (no real internal data, no server keys, etc). All the controlled files need to be avoided from git (by using the ``.gitignore'' file), should they be required in the development of the project.

  For this to be possible, there will be extra workstations connected to an external WiFi network, that does not have access to the internal network and thus, no limitations. This allows for full productivity on the development of the project, without necessity to worry with IT policies and being able to use the best practices and tools.

  \subsection{Issues and Milestones planning} \label{chap5:issues_and_milestones_planning}

  The main project was basically divided into 2 Milestones: the setup and initial repository and the first release. There were created other milestones for other apps as well, but are not shown here.

  The list of milestones can be seen in the image below:

  \begin{figure}[ht]
    \centering
      \includegraphics[scale=0.3]{images/All Milestones.png}
    \caption{List of Milestones \label{fig:all_milestones}}
    \legend{This image was taken on the second of July, therefore the project's status is from his date.}
  \end{figure}
