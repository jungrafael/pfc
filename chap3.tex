\chapter{Technologies and technical basis}

This chapter presents all technologies and paradigms mentioned throughout this document.

It is structured in 3 sections, where the first and the second list the technologies and the third explains how the engino application architecture was planned.

\section{Technologies and methodologies}

Here are introduced all the technologies and methodologies from the planning phase and/or that supported the development process as a whole.

\subsection{Agile development} \label{agile_development}

The Agile Software Development Manifesto \cite{AgileManifesto.Homepage} was created to overcome the big overhead older methodologies caused on software development. The whole idea is to focus  on the development of the software itself instead of too much in its methodologies and documentation.

The four most important principles are:
\begin{itemize}
  \item \textbf{Individuals and Interactions over processes and tools:} Tools and processes are important, but it is more important to have competent people working together effectively;
  \item \textbf{Working Software over comprehensive documentation:} Good documentation is useful in helping people to understand how the software is built and how to use it, but the main point of development is to create software, not documentation;
  \item \textbf{Customer Collaboration over contract negotiation:} A contract is important but is no substitute for working closely with customers to discover what they need;
  \item \textbf{Responding to Change over following a plan:} A project plan is important, but it must not be too rigid to accommodate changes in technology or the environment, stakeholders' priorities, and people's understanding of the problem and its solution.
\end{itemize}

Since the Agile Manifesto, there has been multiple Agile Development methods, frameworks, etc. created and it has received the enormous support from the software development community.

Since it greatly reduces overhead and focuses on features, it works really well with web development, with fast paced development processes, where sometimes the requirements are constantly changing.

With the increase of usage in Agile methodologies and they becoming a standard in software teams, many tools and practices were created to help support the implementation and daily work.

Below are some examples of such tools and practices, as described from two main Git based software providers: Atlassian \cite{Atlassian.CI-CD} and GitLab \cite{GitLab.CI-CD}

  \subsubsection{Continuous Integration} \label{continous_integration}

    When working in teams, many times more than one person is editing the source code of the software at the same time.
    Continuous Integration is a software development practice in which you build and test software every time a developer commits and pushes code to the main repository. This normally happens several times a day, especially in bigger teams.
    This works really well with tools like Git and GitLab, which make the process of merging much simpler and intelligent.

    Some of the best practices are listed below:
    \begin{itemize}
      \item Use a source control system: nowadays normally used together with Git;
      \item Make the build self-testing: the process that build the software should test before building if it works as expected;
      \item Keep the build fast: the build process should be completed in minutes, not hours;
      \item Every commit (to baseline) should be built: the test process should be executed for every commit to the source control system;
      \item Automate deployment: always deploy the merged software to a pre-production server, for final testing and eventually, bug-fixes (\ref{continous_deployment}).
    \end{itemize}

    Workflow Continuous Integration: TEST -> BUILD

  \subsubsection{Continuous Delivery} \label{continuous_delivery}

    Similarly to Continuous Integration [\ref{continous_integration}], Continuous Delivery is a software engineering approach in which continuous integration, automated testing, and automated deployment capabilities allow software to be developed and deployed rapidly, reliably and repeatedly with minimal human intervention. Still, the deployment to production is defined strategically and triggered manually.

    Workflow Continuous Delivery: TEST -> BUILD -> DEPLOY \faHandPointerO (manual)

  \subsubsection{Continuous Deployment} \label{continous_deployment}

    In Continuous Deployment, the code is put into production automatically, resulting in many production deployments every day. It does everything that Continuous Delivery [\ref{continuous_delivery}] does, but the process is fully automated, there's no human intervention at all.

    Workflow Continuous Deployment: TEST -> BUILD -> DEPLOY \faCogs (automatic)

  \subsubsection{Test Driven Development - TDD}

  Test Driven Development is a practice where the tests are created before the actual software. This practice works really well for very specific pieces of software, that must fulfil some very well defined requirements. For example import scripts.

  The development workflow follows:
  \begin{itemize}
    \item Add a test and check if tests start to fail;
    \item Develop feature:
    \begin{itemize}
      \item Write the new code;
      \item Run all tests;
      \item Repeat until tests pass.
    \end{itemize}
    \item Repeat process until feature is completely implemented.
  \end{itemize}

  More information about Test Driven Development can be found in the url \url{http://agiledata.org/essays/tdd.html}.

\subsection{Domain-Driven Design - DDD} \label{ssec:ddd}

Domain-Driven Design was initially introduced and made popular by Eric Evans in 2004 \cite{EricEvans.DDD}. It is an approach for software development by decomposing complex software into understandable and manageable pieces. The modeling is done based on the reality of business as relevant to our use cases. DDD describes independent steps/areas of problems as bounded contexts, emphasizes a common language to talk about these problems.

The main concepts defined by DDD are:

\begin{itemize}
  \item Context: The setting in which a word or statement appears that determines its meaning. Statements about a model can only be understood in a context;
  \item Model: A system of abstractions that describes selected aspects of a domain and can be used to solve problems related to that domain;
  \item Ubiquitous Language: A language structured around the domain model and used by all team members to connect all the activities of the team with the software;
  \item Bounded Context: A description of a boundary (typically a subsystem, or the work of a specific team) within which a particular model is defined and applicable.
\end{itemize}

Tough this pattern makes the software much less complex, by dividing monoliths \footnote{A monolithic application describes a single-tiered software application in which the user interface and data access code are combined into a single program from a single platform} into many smaller pieces, it has the disadvantage of requiring a lot of time for the domains to be throughout understood and adding a lot of overhead because of its separation. It should only be used when the Domain is really well defined and very complex.

\subsection{Microservices}

Software projects can get very big with time. Thus, very difficult to maintain. The whole idea of Microservices comes all the way back from the unix architecture, where all softwares are small, have a well defined objective/context and can really easily exchange messages, through a well defined bridge.

Microservices are originally used in completely independent software, that connect to each other over the network. But its ideas of bounded context separation can be applied also inside one single software.

The ideas behind Microservices are many times shared with that of \nameref{ssec:ddd}.

More information about Microservices can be found on the website \url{http://microservices.io/}.

\subsection{Git}

Git was created by Linus Torvalds in 2005 for development of the Linux kernel. It's popularized to a level, where it's considered the standard among software development.

As described in the Git website \cite{Git.Homepage}:

\begin{quotation}
  \say{Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.}
\end{quotation}

\subsubsection{GitLab}

GitLab is a web-based Git-repository manager with wiki and issue-tracking features, using an open-source license, developed by Dmitriy Zaporozhets and Valery Sizov under GitLab Inc. As stated on the website \cite{GitLab.Homepage}:

\begin{quotation}
  \say{GitLab is the first single application built from the ground up for all stages of the DevOps lifecycle for Product, Development, QA, Security, and Operations teams to work concurrently on the same project. GitLab enables teams to collaborate and work from a single conversation, instead of managing multiple threads across disparate tools. GitLab provides teams a single data store, one user interface, and one permission model across the DevOps lifecycle allowing teams to collaborate, significantly reducing cycle time and focus exclusively on building great software quickly.}
\end{quotation}

The code written in Ruby on Rails, with some parts later rewritten in Go. The core is completely open source, with the MIT License, enabling users to have an instance of the platform running on premises.

One of the biggest competitors from GitLab is GitHub \footnote{GitHub is a similar platform, but is better known because it was the first of its kind in the market}.

\section{Application technologies}

This subsection list all paradigms and technologies related strictly to the application development itself.

\subsection{JSON} \label{ssec:json}

Javascript Object Notation (JSON) is a lightweight data-interchange format. The idea was to make it easy for humans to read and write and easy for machines to parse and generate at the same time as stated on the website \cite{JSON.Homepage}. JSON is a text format that is completely language independent, making it an ideal for data-interchange language.

Due to its simplicity and since it is so integrated with javascript, JSON has become the standard for web applications, specially when sending very small specific data.

JSON has less features than other formats like XML for example. But those feature are most of the time not needed and may only add overhead.

\subsection{SPA} \label{spa}
SPAs, short term for Single Page Applications are web applications or web sites that interacts with the user by dynamically changing the current page, via Javascript, rather than loading entire new pages from a server.

Basically, there is only one initial request to the webserver, which includes the whole layout and frontend features bundled. The subsequent requests are then only very fast and lightweight, with only exchange of specific information. These requests are then read by the Javascript and the necessary DOM\footnote{\label{dom}The Document Object Model is a cross-platform and language-independent application programming interface that treats an HTML, XHTML, or XML document as a tree structure wherein each node is an object representing a part of the document} is altered. They are normally in the \nameref{ssec:json} format.

This approach makes web applications really fast and responsive, since the whole layout is managed only on the client side. As the requests are small, they are executed very fast.

These kind of applications started to get common with the release of \nameref{sssec:angular_js} in 2010.

More information about Single Page Applications can be found on the website \url{http://singlepageappbook.com/goal.html}

\subsubsection{Angular JS} \label{sssec:angular_js}

AngularJS is an open-source front-end web application framework, under the MIT License, created by Google \cite{AngularJS.Homepage} to address many challenges developing SPAs [\ref{spa}].

The framework handles all of the DOM and AJAX glue code and puts it in a well-defined structure. This makes the framework categorized as opinionated.

Some main characteristics are worth listing:

\begin{itemize}
  \item Opinionated: it comes with all libraries included, so less decisions needs to be done at the cost of flexibility;
  \item Two-way data binding: view updates automatically when data model changed. This can cause some undesirable side-effects;
  \item Performance: since all changes in the state are propagated directly to the DOM, AngularJS can be slow if too much data is updated.
\end{itemize}

\subsubsection{React}

React \cite{React.Homepage} is a view library (to build user interfaces) that facilitates the development of Single Page Applications [\ref{spa}]. It was created by Jordan Walke, a software engineer at Facebook and open sourced in 2013 and is today under the MIT License.

React became really popular because breaks through some of the features of \nameref{sssec:angular_js} with much better approaches. For example using one-way data binding, instead of two-way \footnote{In Two-way data binding, view (UI part) updates automatically when data model changed. In one way data binding, this does not happen and we need to write custom code to make for updating the UI. Two-way might be simpler to use on the beginning, but much harder to debug applications and can account to many future problems as the applications grow}. There is also the virtual DOM, where React creates an in-memory data structure cache, computes the resulting differences, and then updates the browser's displayed DOM\footnoteref{dom} efficiently.

\subsection{Functional programming}

Functional programming is a programming paradigm, that avoids state-changing and mutable data. These are also referred to as side effects. Eliminating side effects can make it much easier to understand and predict the behavior of a program, which is one of the key motivations for the development of functional programming.

Programming in a functional style can also be accomplished in languages that are not specifically designed for functional programming. In the snipped below is an example of Higher Order Functions written in Java 8 \cite{Medium.FunctionalProgrammingInJava}. Since the language was not created with these concepts in mind, the syntax is really bad. But that's inherent from the Java programming language.

\begin{minted}{java}
@FunctionalInterface
interface DogAge {
    Integer apply(Dog dog);
}


List<Integer> getAges(List<Dog> dogs, DogAge f) {
    List<Integer> ages = new ArrayList<>();

    for (Dog dog : dogs) {
        ages.add(f.apply(dog));
    }

    return ages;
}
\end{minted}


\subsection{Erlang}

Erlang \cite{Erlang.Homepage} is a general-purpose, concurrent, functional programming language, as with a garbage-collection runtime system. The Erlang Runtime System (OTP) consists of a number of ready-to-use components mainly written in Erlang, and a set of design principles for Erlang programs.

It was originally a proprietary language within Ericsson, developed by Joe Armstrong, Robert Virding and Mike Williams in 1986, but released as open source in 1998.

The main features of the languages are:
\begin{itemize}
  \item Distributed;
  \item Fault-tolerant;
  \item Soft real-time;
  \item Highly available, non-stop applications;
  \item Hot swapping, where code can be changed without stopping a system.
\end{itemize}

The Erlang BEAM Virtual Machine executes bytecode which is converted to threaded code at load time. It also includes a native code compiler on most platforms, developed by the High Performance Erlang Project (HiPE) at Uppsala University. It is now fully integrated in Ericsson's Open Source Erlang/OTP system.

\subsubsection{Elixir}

Elixir \cite{Elixir.Homepage} is a functional, concurrent, general-purpose programming language that runs on the Erlang (BEAM) virtual machine. It can be used to create distributed, fault-tolerant, soft-real time, and permanently-running programs.

Elixir supports compile-time metaprogramming with macros and polymorphism via protocols, enabling the language's API to be easily extended.

\subsubsection{Phoenix Framework} \label{phoenix_framework}

\epigraph{
  Productive. Reliable. Fast.
  A productive web framework that
  does not compromise speed and maintainability. \cite{PhoenixFramework.Website}
}{\textit{Phoenix Framework}}

Phoenix is a web development framework written in Elixir. It uses a server-side model-view-controller (MVC) pattern and is strongly based in the Ruby on Rails approach for building web application.

\subsection{GraphQL}

GraphQL \cite{GraphQL.Homepage} is a data query language developed internally by Facebook in 2012 and open sourced in 2015, under the MIT License.

This query language was developed to solve some of the biggest problems from REST APIs\footnote{REST is an architectural style that defines a set of properties based on the HTTP protocol. It is used for data exchange between systems.} in bigger applications. The most common are:

\begin{itemize}
  \item The need to do multiple round trips to fetch data required by a view;
  \item Clients dependency on servers;
  \item The bad front-end developer experience.
\end{itemize}

\subsection{ECMAScript}

ECMAScript is a trademarked scripting-language specification, created to standardize JavaScript, so as to foster multiple independent implementations. JavaScript has remained the best-known implementation of ECMAScript since the standard was first published in 1997 \cite{ECMASCript.FirstRelease}. ECMAScript is commonly used for client-side scripting on the World Wide Web, and it is increasingly being used for writing server applications and services using Node.js \footnote{Node.js is simply the Google V8 engine - a javascript interpreter used by Google Chrome® - bundled with some libraries to do I/O and networking}.

The sixth edition, known as ECMAScript 6 (ES6) or ECMAScript 2015 (ES2015) \cite{ECMASCript.Specification} adds significant new syntax for writing complex applications, including classes and modules. Other new features include iterators and for/of loops, Python-style generators and generator expressions, arrow functions, binary data, typed arrays, collections (maps, sets and weak maps), promises, number and math enhancements, reflection, and proxies (metaprogramming for virtual objects and wrappers). These new features revolutionized Javascript development, enabling the creation of much more complex applications.

\subsubsection{Webpack} \label{webpack}

Webpack \cite{Webpack.Website} is an open-source JavaScript module bundler, under the MIT License. It takes modules with dependencies and generates static assets representing those modules. Since Webpack compiles all the code developed into a bundle, it is commonly used for converting new technologies to be supported in older browsers, as well as some many speed optimizations.

The Figure \ref{fig:webpack_inout_image} gives an overview on the idea behind the creation of Webpack:

\begin{figure}[ht]
  \centering
    \includegraphics[scale=0.4]{images/3.2.6.1 - Webpack.png}
  \caption{Webpack \label{fig:webpack_inout_image}}
  \legend{Source: Webpack Website \cite{Webpack.Website}}
\end{figure}

\section{Engino architecture}

Engino is the first application that will be developed using the platform described in this project. An overview about the project was mentioned in Chapter 2 [\ref{chapter2engino}].

Since the application was a rebuild from the old application and the requirements are constantly changing, the biggest requirement was really being as flexible and modular as possible.

An Agile approach was in this case fundamental. And by using the latest web technologies, it helped to have the application very modular and easy to be changed in the future.

\subsection{Frontend}

To make the software as flexible as possible, the interfaces should be built completely modular and as reusable as possible.

For this reason, the frontend stack was based around the React library (more information in \ref{ssec:engino_requirements}), with the following libraries/technologies:

\begin{itemize}
  \item \textbf{React}: Main view library;
  \item \textbf{Redux}: Handles state management;
  \item \textbf{React Router}: For generating and managing the routes;
  \item \textbf{Material UI}: React Components that Implement Google's Material Design.;
  \item \textbf{React Leaflet}: For implementing the Mapping functionality;
  \item \textbf{Superagent}: A REST Client for Javascript.
\end{itemize}

\subsection{API}

Since there should be multiple applications being glued together, the connection between backend and frontend should also be as flexible as possible. Knowing the problems with REST APIs, the newer GraphQL query format interface was chosen to integrate the application with the server.

\begin{itemize}
  \item The server uses the Elixir Library Absinthe, which makes a GraphQL API Endpoint;
  \item On the client side, the library used is Apollo, for connecting to the GraphQL API Endpoint;
  \item To connect the backend and frontend together, the communication will be done through websockets, for enabling real time updates on the applications.
\end{itemize}

\subsection{Backend}

The backend is composed basically by the platform Erlang/OTP, where code is written in Elixir and compiled into it.

Multiple libraries are used for the backend applications:
\begin{itemize}
  \item \textbf{Elixir}: The language of the server applications;
  \item \textbf{Ecto}: The database wrapper library;
  \item \textbf{Phoenix}: The Elixir web application framework, to help developing the web facing applications;
  \item \textbf{Postgrex}: Elixir library to help connecting to the PostgreSQL database;
  \item \textbf{Cowboy}: The web server, that receives all the http requests;
  \item \textbf{Exldap}: Library to help making authentication through the internal LDAP servers (Microsoft Active Directory);
  \item \textbf{Tesla}: HTTP Client, to connect to external services;
  \item \textbf{Honeydew}: Library to manage worker pools and queues, for concurrent batch processing.
\end{itemize}

\subsection{Server software} \label{ssec:server_software}

The server is defined by the internal IT from Rolls-Royce. The server available is a Red Hat Enterprise Linux, running in a virtual machine. Since there is no root access available, most software needed to be pre-compiled into binaries, that would run dependency-free on user space on the system. These software required by the Engino Backend application are:

\begin{itemize}
  \item \textbf{PostgreSQL}: The internal database for the system;
  \item \textbf{ImageMagick}: Image processing library used;
  \item \textbf{Ghostscript}: Utility to read and process PDF files;
  \item \textbf{RSVG}: Utility to interpret SVG files;
  \item \textbf{Oracle Instant Client}: Proprietary tool to connect to the Oracle Databases.
\end{itemize}
