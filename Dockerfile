FROM sumdoc/texlive-2017

RUN apt-get update && apt-get -y upgrade
RUN apt-get install unzip
RUN cd /tmp && wget https://www.tug.org/fonts/getnonfreefonts/install-getnonfreefonts \
 && texlua install-getnonfreefonts \
 && getnonfreefonts --sys -a

RUN apt-get update && apt-get -y upgrade && apt-get install -y python-pip
RUN pip install Pygments
